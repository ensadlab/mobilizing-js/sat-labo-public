export const playerSchema = {
    id: {
        type: 'integer',
        default: null,
        nullable: true,
    },

    acc: {
        type: 'any',//{x:0, y:0, z:0}
        default: { x: 0, y: 0, z: 0 },
        //nullable: true,
    },

    quaternion: {
        type: 'any',//{x:0, y:0, z:0}
        default: { x: 0, y: 0, z: 0, w: 0 },
        //nullable: true,
    },
};

export default playerSchema;
