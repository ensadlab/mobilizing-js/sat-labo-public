import * as Mobilizing from '@mobilizing/library';
import { Client } from '@mobilizing/platform/client/Client.js';
import { DisplayScript } from './DisplayScript.js';

export class DisplayClient extends Client {
    constructor({
        features,
        container,
    } = {}) {
        super({
            features,
            container,
        });
        this.client = this;
        this.container = container;
        this.rafId = null;

        this.context = new Mobilizing.Context();
        this.instance = new DisplayScript({
            reportCallback: (updates) => this.report(updates), // bind this
        });

        this.playerStates = new Map();
    }

    async init() {
        await super.init();
        // features may be available
    }

    async start() {
        await super.start();
        // available features should be ready

        // move soundworks container under
        this.container.style.zIndex = -100;
        this.container.style.position = 'absolute';

        // register to the already existing global state
        this.globalState = await this.client.stateManager.attach('global');

        //subscribe to state updates
        this.globalState.subscribe((updates) => {
            const { reload } = updates;
            if (reload) {
                window.location = window.location; // eslint-disable-line
            }
            //meaAcc update
            const { meanAcc } = updates;
            if (meanAcc) {
                this.instance.updateMeanBrick({ meanAcc });
            }
        });

        this.client.stateManager.observe(async (schemaName, stateId, nodeId) => {

            switch (schemaName) {

                case "player": {

                    const state = await this.client.stateManager.attach(schemaName, stateId);
                    //console.log(state);

                    //player disappears
                    state.onDetach(() => {
                        this.instance.removePlayer(stateId);
                        this.playerStates.delete(stateId);
                    });
                    //subscribe to updates coming from shared states
                    state.subscribe((updates) => {
                        //console.log(updates);
                        this.instance.updatePlayer(stateId, updates);

                        if (this.playerStates.size > 0) {

                            const meanQuat = new Mobilizing.three.Quaternion();
                            let playerLoopIndex = 0;
                            const weight = 1 / this.playerStates.size;

                            this.playerStates.forEach((playerState) => {

                                const tempQuaternion = playerState.get("quaternion");

                                /*  if (playerLoopIndex === 0) {
                                     meanQuat.set(tempQuaternion.x, tempQuaternion.y, tempQuaternion.z, tempQuaternion.w);
                                 }
                                 else { */
                                const quaternion = new Mobilizing.three.Quaternion(tempQuaternion.x, tempQuaternion.y, tempQuaternion.z, tempQuaternion.w);

                                const q = new Mobilizing.three.Quaternion(0, 0, 0, 1);
                                q.slerp(quaternion, weight);
                                meanQuat.multiply(q);
                                //}
                                //  playerLoopIndex++;
                            });

                            this.instance.updateMeanBrick({ meanQuat });
                            //console.log(meanQuat);
                        }
                    });
                    //adds new skeleton to the main map
                    this.instance.addPlayer(stateId, state);

                    this.playerStates.set(stateId, state);

                    break;
                }
                case "livePose": {

                    const state = await this.client.stateManager.attach(schemaName, stateId);

                    //skeletons disappears
                    state.onDetach(() => {
                        this.instance.removeSkeleton(stateId);
                    });

                    //subscribe to updates coming from shared states
                    state.subscribe((updates) => {
                        this.instance.updateSkeleton(stateId, updates);
                    });

                    //adds new skeleton to the main map
                    this.instance.addSkeleton(stateId, state);
                    break;
                }
                default:
                    break;
            }
        });

        //check the script for the need of user interaction
        if (this.instance.needsUserInteraction) {
            this.context.userInteractionDone.setup();
            await this.context.userInteractionDone.promise;
        }

        this.context.addComponent(this.instance);
        this.runner = new Mobilizing.Runner({
            context: this.context,
        });
    }

    //for data and events coming from Mobilizing user script
    report(updates) {

    }

}

export default DisplayClient;
