import * as Mobilizing from '@mobilizing/library';

export class PlayerScript {

    constructor({
        reportCallback,
    } = {}) {
        this.reportCallback = reportCallback;
        this.needsUserInteraction = true;
    }

    preLoad(loader) {
    }

    setup() {

        this.audioRenderer = new Mobilizing.audio.Renderer();
        this.context.addComponent(this.audioRenderer);
        this.audioRenderer.setup();

        //We take advantage of the Basic3DContext class to get a predefined scene
        Mobilizing.Basic3DContext.makeBasicContext(this.context);
        //this.context.camera.setToPixel();
        this.context.camera.transform.setLocalPositionZ(10);

        this.pointer = this.context.pointer;

        console.log("this.pointer", this.pointer);

        this.accel = new Mobilizing.input.Motion();
        this.accel = this.context.addComponent(this.accel);
        this.accel.setup(); //set it up
        this.accel.on(); //active it

        this.accel.events.on("accelerationGravityVector", this.accelEvent.bind(this));

        //inputs
        this.orientation = new Mobilizing.input.Orientation();
        this.context.addComponent(this.orientation);
        this.orientation.setup();
        this.orientation.on();

        this.orientation.events.on("deviceorientation", this.orientationEvent.bind(this));

        this.gyroQuat = new Mobilizing.three.Quaternion();

        const light = new Mobilizing.three.Light();
        light.transform.setLocalPosition(10, 10, 10);
        this.context.renderer.addToCurrentScene(light);

        this.brick = new Mobilizing.three.Box({
            "width": 3,
            "height": 4,
            "depth": .5
        });
        this.brick.transform.setLocalPosition(0, 0, 0);
        this.context.renderer.addToCurrentScene(this.brick);
    }

    accelEvent(acc) {
        //let accel = this.accel.getSmoothedAcc(.07);
        /* this.brick.transform.setLocalRotationOrder("ZXY");
        this.brick.transform.setLocalRotation(acc.z, -acc.x, -acc.y + 180); */

        this.report({ acc });
    }

    orientationEvent(compass) {

        this.gyroQuat.setFromGyro(compass);
        if (this.gyroQuat) {
            this.brick.transform.setLocalQuaternion(this.gyroQuat);

            const quaternion = {
                x: this.gyroQuat.x,
                y: this.gyroQuat.y,
                z: this.gyroQuat.z,
                w: this.gyroQuat.w
            };
            this.report({ quaternion });
        }
    }

    update() {

    }

    apply(/* {} = {} */) {

    }

    report(updates) {
        if (!this.reportCallback) {
            return;
        }

        // direct binding
        this.reportCallback(updates);
    }

}
