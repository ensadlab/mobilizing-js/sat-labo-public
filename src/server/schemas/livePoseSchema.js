export const livePoseSchema = {

    camIndex: {
        type: "string",
        default: null,
        nullable: true,
    },

    poseIndex: {
        type: "string",
        default: null,
        nullable: true,
    },

    skeleton: {
        type: "any",
        default: null,
        nullable: true,
    },
}

export default livePoseSchema;
