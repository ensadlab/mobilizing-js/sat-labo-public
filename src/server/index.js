/* eslint-disable no-await-in-loop */
/* eslint-disable guard-for-in */
import 'regenerator-runtime/runtime';
import 'source-map-support/register';

import { Server } from '@mobilizing/platform/server/Server.js';
import globalSchema from './schemas/globalSchema.js';
import playerSchema from './schemas/playerSchema.js';
import livePoseSchema from './schemas/livePoseSchema.js';

import { createServer } from 'https';
import { readFileSync } from 'fs';
import { WebSocketServer } from 'ws';

import { LivePoseObjectParser } from '../livePoseUtils/LivePoseObjectParser';

const server = new Server();

// avoid server crash
process.on('unhandledRejection', (reason, p) => {
    console.log('> Unhandled Promise Rejection');
    console.log(reason);
});

// function to allow for async
(async function launch() {
    try {
        // -------------------
        // launch application
        // -------------------
        await server.init();
        await server.start();

        //register schema    
        server.stateManager.registerSchema('global', globalSchema);
        server.stateManager.registerSchema('player', playerSchema);
        server.stateManager.registerSchema('livePose', livePoseSchema);

        // single instance of global schema, created here
        const globalState = await server.stateManager.create('global');

        // -------------------
        // WebSocket for LivePose
        // -------------------
        //create a new server with credential files
        const WSServer = createServer({
            cert: readFileSync(server.configuration.env.httpsInfos.cert),
            key: readFileSync(server.configuration.env.httpsInfos.key)
        });
        //change the port if needed in ./config/env/default.json
        const webSocketPort = server.configuration.env.websockets.LivePoseWebSocketPort;

        const webSocketServer = new WebSocketServer({
            server: WSServer
        });

        //Dict to handle States from LivePose incoming JSON
        const skeletonsStates = new Set();

        webSocketServer.on('connection', (ws) => {

            ws.on('message', async (data) => {
                //console.log('received: %s', data, "\n");

                const jsonData = data.toString();

                //convert raw data to object
                LivePoseObjectParser.parseLivePoseStream(jsonData);

                const skeletonsFromStream = LivePoseObjectParser.getSkeletons();
                //console.log("skeletonsFromStream", skeletonsFromStream, "\n");

                //first shot : add everything
                if (skeletonsStates.size === 0) {

                    //skeletonsFromStream.forEach( async (skeletonFromStreamItem) => {
                    for (const skeletonFromStreamItem of skeletonsFromStream) {
                        // console.log("first connection");
                        // console.log("skeletonFromStreamItem", skeletonFromStreamItem);

                        //flag for deleting on next frame is needed
                        skeletonFromStreamItem.toDelete = false;
                        //skeletons.add(skeletonFromStreamItem);

                        const skeletonState = await server.stateManager.create('livePose', {
                            camIndex: skeletonFromStreamItem.camIndex,
                            poseIndex: skeletonFromStreamItem.poseIndex,
                            skeleton: skeletonFromStreamItem.skeleton
                        });

                        skeletonsStates.add(skeletonState);
                        //console.log("skeletonState", skeletonState);
                    }
                }
                //);

                //not the first shot, delete or add :
                else {
                    //search for deleting needs
                    skeletonsStates.forEach((skeletonItem) => {

                        skeletonItem.toDelete = true;

                        skeletonsFromStream.forEach((skeletonFromStreamItem) => {

                            const skeletonAreEquals = (
                                skeletonItem.get("camIndex") === skeletonFromStreamItem.camIndex &&
                                skeletonItem.get("poseIndex") === skeletonFromStreamItem.poseIndex
                            );

                            if (skeletonAreEquals) {
                                skeletonItem.toDelete = false;
                            }
                        });
                    });

                    //console.log(skeletonsStates);

                    //then delete it if found
                    skeletonsStates.forEach((skeletonItem) => {
                        if (skeletonItem.toDelete) {
                            skeletonsStates.delete(skeletonItem);
                            //delete from stateManager to mimic a disconnection
                            skeletonItem.delete();
                        }
                    });

                    //flag incoming obj
                    skeletonsFromStream.forEach((skeletonFromStreamItem) => {
                        skeletonFromStreamItem.toAdd = true;
                    });

                    //search incoming object for skeleton to add
                    //AND update the skeletons values for existing ones
                    skeletonsFromStream.forEach((skeletonFromStreamItem) => {
                        skeletonsStates.forEach((skeletonItem) => {

                            const skeletonAreEquals = (
                                skeletonItem.get("camIndex") === skeletonFromStreamItem.camIndex &&
                                skeletonItem.get("poseIndex") === skeletonFromStreamItem.poseIndex
                            );

                            if (skeletonAreEquals) {
                                skeletonFromStreamItem.toAdd = false;
                                //!!update skeleton value!!
                                skeletonItem.set({ skeleton: skeletonFromStreamItem.skeleton });
                            }
                        });
                    });

                    //then add them if found
                    //skeletonsFromStream.forEach(async (skeletonFromStreamItem) => {
                    for (const skeletonFromStreamItem of skeletonsFromStream) {

                        if (skeletonFromStreamItem.toAdd) {

                            const skeletonState = await server.stateManager.create('livePose', {
                                camIndex: skeletonFromStreamItem.camIndex,
                                poseIndex: skeletonFromStreamItem.poseIndex,
                                skeleton: skeletonFromStreamItem.skeleton
                            });
                            skeletonsStates.add(skeletonState);
                        }
                    }
                    //});
                }

                /* const debugSkeletonsStates = Array.from(skeletonsStates.values());
                debugSkeletonsStates.map( (state, index) => {
                    console.log("index : ", index, state.getValues());
                }); */

            });
        });

        WSServer.listen(webSocketPort);

        //playerStates
        const playerStates = new Set();

        const preLoad = (playerState) => {
            // nothing before preLoad

            playerState.set({
                preLoad: true,
            });
        };

        // new client
        server.stateManager.observe(async (schemaName, stateId, nodeId) => {
            switch (schemaName) {
                case 'player': {
                    // new player client is created client-side
                    // attach server-side
                    const playerState = await server.stateManager.attach(schemaName, stateId);

                    // keep a set of all players
                    playerStates.add(playerState);

                    playerState.subscribe((updates) => {

                        if (playerStates.size > 0) {
                            const meanAcc = { x: 0, y: 0, z: 0 };
                            
                            playerStates.forEach((state) => {
                                const acc = state.get("acc");
                                meanAcc.x += acc.x;
                                meanAcc.y += acc.y;
                                meanAcc.z += acc.z;
                            });

                            meanAcc.x /= playerStates.size;
                            meanAcc.y /= playerStates.size;
                            meanAcc.z /= playerStates.size;

                            if (playerStates.size === 0) {
                                return;
                            }

                            globalState.set({ meanAcc });
                        }
                        //console.log(globalState.get("meanAcc"));

                    }); // udpdates

                    // logic to do when the state is deleted
                    // (e.g. when the player disconnects)
                    playerState.onDetach(() => {
                        // clean things
                        playerStates.delete(playerState);
                    });

                    break;
                } // 'player'

                default:
                    break;

            }
        });

        // previously created server-side
        globalState.subscribe((updates) => {
            // nothing here
        });

    }
    catch (err) {
        console.error(err.stack);
    }
})();

