import * as Mobilizing from '@mobilizing/library';

export class DisplayScript {

    constructor({
        reportCallback,
    } = {}) {

        this.reportCallback = reportCallback;
        //this.needsUserInteraction = true;

        //livePose Skeletons
        this.skeletons = new Map();

        //mobile bricks
        this.bricks = new Map();

        //grid position management
        this.bricksMaxColumns = 4;
        this.bricksMaxLines = 4;
        this.bricksSlots = [];
        for (let y = 0; y < this.bricksMaxLines; y++) {
            const bricksLine = [];
            for (let x = 0; x < this.bricksMaxColumns; x++) {
                bricksLine.push(false);
            }
            this.bricksSlots.push(bricksLine);
        }
        //console.log(this.bricksSlots);

        this.brickWidth = 30;
        this.brickHeight = 50;

        this.brickStep = this.brickWidth * 4;

        this.bricksStartPosition = new Mobilizing.three.Vector3(
            - (this.brickStep * (this.bricksMaxColumns - 1)) / 2,
            (this.brickStep * (this.bricksMaxLines - 1)) / 2,
            0);

        this.currentBrickXIndex = 0;
        this.currentBrickYIndex = 0;
    }

    preLoad(loader) {
    }

    setup() {
        //We take advantage of the Basic3DContext class to get a predefined scene
        Mobilizing.Basic3DContext.makeBasicContext(this.context);
        //this.context.camera.setToPixel();
        this.context.camera.transform.setLocalPositionZ(1000);
        //this.context.renderer.setClearColor(Mobilizing.three.Color.red);
        this.pointer = this.context.pointer;

        //audio
        this.audioRenderer = new Mobilizing.audio.Renderer();
        this.context.addComponent(this.audioRenderer);
        this.audioRenderer.setup();

        this.meanBrick = new Mobilizing.three.Box({
            "width": this.brickWidth * 5,
            "height": this.brickHeight * 5,
            "depth": (this.brickWidth * 5) / 4,
            "material": "basic"
        });
        this.meanBrick.transform.setLocalPosition(0, 0, 5);
        this.meanBrick.material.setWireframe(true);
        this.context.renderer.addToCurrentScene(this.meanBrick);
    }

    update() {
    }

    /*=============
    *  Players (mobile)
    * =============
    */
    removePlayer(stateId) {
        const brick = this.bricks.get(stateId);

        //free the slots used by brick in grid
        const xIndex = brick.xIndex;
        const yIndex = brick.yIndex;

        this.bricksSlots[xIndex][yIndex] = false;

        this.context.renderer.removeFromCurrentScene(brick);
        this.bricks.delete(stateId);
    }

    updatePlayer(stateId, updates) {
        //console.log(updates);
        const { acc, quaternion } = updates;
        const brick = this.bricks.get(stateId);
        /* brick.transform.setLocalRotationOrder("ZXY");
        brick.transform.setLocalRotation(acc.z, -acc.x, -acc.y + 180); */
        if (quaternion) {
            const gyroQuat = new Mobilizing.three.Quaternion(
                quaternion.x,
                quaternion.y,
                quaternion.z,
                quaternion.w
            );
            brick.transform.setLocalQuaternion(gyroQuat);
        }
    }

    addPlayer(stateId, state) {

        //manage grid base organisation with an abstract list of available slots
        let xIndex = null;
        let yIndex = null;
        let shouldBreak = false;

        for (let i = 0; i < this.bricksMaxLines; i++) {
            for (let j = 0; j < this.bricksMaxColumns; j++) {

                if (!this.bricksSlots[i][j]) {
                    this.bricksSlots[i][j] = true;
                    xIndex = i;
                    yIndex = j;
                    shouldBreak = true;
                    break;
                }
            }
            if (shouldBreak) {
                break;
            }
        }
        //console.log("xIndex", xIndex, "yIndex", yIndex, this.bricksSlots);

        const brick = new Mobilizing.three.Box({
            "width": this.brickWidth,
            "height": this.brickHeight,
            "depth": this.brickWidth / 4
        });

        brick.xIndex = xIndex;
        brick.yIndex = yIndex;

        const x = this.bricksStartPosition.x + yIndex * this.brickStep;
        const y = this.bricksStartPosition.y - xIndex * this.brickStep * 1.2;
        //console.log("x", x, "y", y);

        brick.transform.setLocalPosition(x, y, 0);
        this.context.renderer.addToCurrentScene(brick);

        this.bricks.set(stateId, brick);

        this.currentBrickIndex++;
    }

    updateMeanBrick(updates) {

        const { meanAcc, meanQuat } = updates;

        if (meanAcc) {
            /*  this.meanBrick.transform.setLocalRotationOrder("ZXY");
             this.meanBrick.transform.setLocalRotation(meanAcc.z, -meanAcc.x, -meanAcc.y + 180); */
        }
        else if (meanQuat) {
            this.meanBrick.transform.setLocalQuaternion(meanQuat);
        }
    }

    /*
    * =============
    *  LivePose Skeletons
    * =============
    */

    removeSkeleton(stateId) {
        const root = this.skeletons.get(stateId);
        this.context.renderer.removeFromCurrentScene(root);
        //TODO : destroy mesh to clear memory
        this.skeletons.delete(stateId);
    }

    addSkeleton(stateId, state) {

        const stateValues = state.getValues();

        const camIndex = stateValues.camIndex;
        const poseIndex = stateValues.poseIndex;
        const skeleton = stateValues.skeleton;
        const pose_id = skeleton.pose_id;

        //flat keypoints array
        const tempKeyPoints = Object.values(skeleton.keypoints);
        //console.log("tempKeyPoints", tempKeyPoints);
        const keyPoints = [];
        tempKeyPoints.forEach((keyPoint) => {
            const point = new Mobilizing.three.Vector3(Number(keyPoint[0]), -Number(keyPoint[1]), 0)
            keyPoints.push(point);
        });

        const root = new Mobilizing.three.Node();
        root.camIndex = camIndex;
        root.poseIndex = poseIndex;
        root.pose_id = pose_id;

        //console.log("keyPoints", keyPoints);
        const mesh = new Mobilizing.three.Segments({
            points: keyPoints
        });
        //console.log(mesh);
        //#0
        root.transform.addChild(mesh.transform);

        const pointCloud = new Mobilizing.three.Points({
            points: keyPoints
        });
        pointCloud.setPointsSize(20);
        //#1
        root.transform.addChild(pointCloud.transform);

        const bbox = pointCloud.getSize();
        const center = pointCloud.getCenter();
        const box = new Mobilizing.three.Box({
            width: bbox.x,
            height: bbox.y,
            length: bbox.z,
            material: "basic"
        });
        box.transform.setLocalPosition(center.x, center.y);
        box.material.setWireframe(true);
        box.material.setColor(Mobilizing.three.Color.red);
        //#2
        //this.context.renderer.addToCurrentScene(box);
        root.transform.addChild(box.transform);

        this.context.renderer.addToCurrentScene(root);
        //add the new skeleton to the storage Map
        this.skeletons.set(stateId, root);
        console.log(this.skeletons);
    }

    updateSkeleton(stateId, updates) {

        const skeleton = updates.skeleton;
        //flat keypoints array
        const tempKeyPoints = Object.values(skeleton.keypoints);
        //console.log("tempKeyPoints", tempKeyPoints);
        const keyPoints = [];
        tempKeyPoints.forEach((keyPoint) => {
            const point = new Mobilizing.three.Vector3(Number(keyPoint[0]), -Number(keyPoint[1]), 0)
            keyPoints.push(point);
        });

        const root = this.skeletons.get(stateId);

        const mesh = root.transform.getChild(0);
        keyPoints.forEach((point, index) => {
            mesh.setPoint(index, point);
        });

        const pointCloud = root.transform.getChild(1);
        keyPoints.forEach((point, index) => {
            pointCloud.setPoint(index, point);
        });

        const bbox = pointCloud.getNativeGeometry();
        bbox.computeBoundingBox();
        const v = new Mobilizing.three.Vector3();
        bbox.boundingBox.getSize(v);
        //console.log("bbox",v);

        let box = root.transform.getChild(2);
        root.transform.removeChild(box.transform);
        box.erase();
        box = null;

        const newBbox = pointCloud.getSize();
        const center = pointCloud.getCenter();
        box = new Mobilizing.three.Box({
            width: newBbox.x,
            height: newBbox.y,
            length: newBbox.z,
            material: "basic"
        });
        box.material.setColor(Mobilizing.three.Color.red);
        box.transform.setLocalPosition(center.x, center.y);
        box.material.setWireframe(true);

        root.transform.addChild(box.transform);

        //console.log(updates);
    }

    report(updates) {
        if (!this.reportCallback) {
            return;
        }

        // direct binding
        this.reportCallback(updates);
    }

}
