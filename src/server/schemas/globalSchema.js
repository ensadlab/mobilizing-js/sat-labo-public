export const globalSchema = {

    reload: {
        type: 'boolean',
        event: true,
    },

    debug: {
        type: 'boolean',
        default: false,
    },

    meanAcc: {
        type: 'any',//{x:0, y:0, z:0}
        default: { x: 0, y: 0, z: 0 },
        nullable: true,
    }
};

export default globalSchema;
